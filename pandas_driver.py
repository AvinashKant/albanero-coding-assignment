import pandas as pd

class PandasDriver:

    def __init__(self, filename):
        self.filename = filename
        self.total_number_of_rows = 0
        self.total_number_of_columns = 0
        self.columns_distinct_values = dict({})
        self.process_data()

    def process_data(self):

        dataframe = pd.read_csv(self.filename,encoding= 'unicode_escape')
        # to handle empty and null issue
        dataframe = dataframe.fillna('')

        #get total rows and columns
        self.total_number_of_rows, self.total_number_of_columns = dataframe.shape

        # get header columns to iterate dataframe object easily 
        header_columns = list(dataframe.columns.values)
        
        for column in header_columns:
            self.columns_distinct_values[column] = pd.unique(dataframe[column]).tolist()
            
    
    def get_total_rows(self):
        return self.total_number_of_rows
    
    def get_total_columns(self):
        return self.total_number_of_columns
        
    def get_columns_distinct_values(self):
        return self.columns_distinct_values
        
    def get_columns_distinct_empty(self):
        return {
            "name": 4,
            "age": 5
        }
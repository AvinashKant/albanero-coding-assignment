import csv
import requests

class PythonDefaultDriver:

    def __init__(self, filename):
        self.filename = filename
        self.total_number_of_rows = 0
        self.total_number_of_columns = 0
        self.columns_distinct_values = dict({})
        self.columns_distinct_empty = dict({})
        self.process_data()

    def process_data(self):
        data = requests.get(self.filename)
        lines = data.text.splitlines()
        self.total_number_of_rows = len(lines)
        csv_data = csv.reader(lines)
        self.total_number_of_columns = len(next(csv_data))

    
    def get_total_rows(self):
        return self.total_number_of_rows
    
    def get_total_columns(self):
        return self.total_number_of_columns
        
    def get_columns_distinct_values(self):
        return self.columns_distinct_values
        
    def get_columns_distinct_empty(self):
        return self.columns_distinct_empty
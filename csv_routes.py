from fastapi import APIRouter, Body, Request, status
from fastapi.encoders import jsonable_encoder
from typing import List
import csv_process_service as csvObj
from csv_statistics_model import CSVStatisticsIn, CSVStatisticsOut

statistics_routes = APIRouter()

@statistics_routes.post("/", response_description="Create a new csv history", status_code=status.HTTP_201_CREATED)
def upload_csv(request: Request, csvFile: CSVStatisticsIn =Body(...)):

    csvFile = jsonable_encoder(csvFile)
    # load csv file with driver
    csvPObj = csvObj.CSVProcessService(csvFile['file_path'],'PandasDriver')

    # append more statistics with processed csv file
    csvFile['total_rows'] = csvPObj.driver_obj.get_total_rows()
    csvFile['total_columns'] = csvPObj.driver_obj.get_total_columns()
    csvFile['columns_distinct_values'] = csvPObj.driver_obj.get_columns_distinct_values()
    csvFile['columns_distinct_empty'] = csvPObj.driver_obj.get_columns_distinct_empty()

    request.app.database["csv_files"].insert_one(csvFile)

    return csvFile


@statistics_routes.get("/{file_name}", response_description="List csv files")
def view_csv_data(file_name: str,request: Request):
    file_data = request.app.database["csv_files"].find({"_id": file_name})
    #file_data = request.app.database["csv_files"].find({"file_path": file_name})
    return list(file_data)




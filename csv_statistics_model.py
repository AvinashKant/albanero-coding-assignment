import uuid
from pydantic import BaseModel, Field

class CSVStatisticsIn(BaseModel):
    id: str = Field(default_factory=uuid.uuid4, alias="_id")
    file_path: str = Field(...)

    class Config:
        allow_population_by_field_name = True
        schema_extra = {
            "example": {
                "file_path":"user.csv"
            }
        }

class CSVStatisticsOut(BaseModel):
    id: str = Field(default_factory=uuid.uuid4, alias="_id")
    file_path: str = Field(...)
    total_rows: int = Field(...)
    total_columns: int = Field(...)
    columns_distinct_values: object = Field(...)
    columns_distinct_empty:  object = Field(...)

    class Config:
        allow_population_by_field_name = False
        schema_extra = {
            "example": {
                "_id": "066de609-b04a-4b30-b46c-32537c7f1f6e",
                "file_path":"user.csv",
                "total_rows": 5,
                "total_columns": 3,
                "columns_distinct_values": {
                    "name":{
                        'Rahul',
                        "Abdul",
                        "Avinash"
                    }
                },
                "columns_distinct_empty":{
                    "name":4,
                    "age":5
                }
            }
        }
import python_default_driver
#import py_spark_driver
import pandas_driver


class CSVProcessService:

  def __init__(self, file_name, driver_type):

    self.driver_obj = None
    self.file_name = file_name    
    self.driver_type = driver_type  
    self.process_csv()

  def process_csv(self):
    self.driver_obj = self.get_driver_obj()
  
  def get_driver_obj(self):
    if(self.driver_type=='PythonDefaultDriver'):
      return python_default_driver.PythonDefaultDriver(self.file_name)
    elif(self.driver_type=='PySparkDriver'):
      return py_spark_driver.PySparkDriver(self.file_name)
    elif(self.driver_type=='PandasDriver'):
      return pandas_driver.PandasDriver(self.file_name) 